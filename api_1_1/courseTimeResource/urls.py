#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import coursetime_blueprint
from api_1_1.courseTimeResource.courseTimeResource import CourseTimeResource
from api_1_1.courseTimeResource.courseTimeOtherResource import CourseTimeOtherResource

api = Api(coursetime_blueprint)

api.add_resource(CourseTimeResource, '/course-time/<autoID>', '/course-time', endpoint='CourseTime')

