#!/usr/bin/env python
# -*- coding:utf-8 -*- 

from flask_restful import Resource, reqparse
from flask import jsonify

from controller.testController import TestController
from utils import commons
from utils.response_code import RET


class TestResource(Resource):

    # get
    @classmethod
    def get(cls, test_id=None):
        if test_id:
            kwargs = {
                'test_id': test_id
            }

            res = TestController.get(**kwargs)
            if res['code'] == RET.OK:
                return jsonify(code=res['code'], message=res['message'], data=res['data'])
            else:
                return jsonify(code=res['code'], message=res['message'], data=res['data'])

        parser = reqparse.RequestParser()
        parser.add_argument('test_context', location='args', required=False, help='test_context参数类型不正确或缺失')
        
        parser.add_argument('Page', location='args', required=False, help='Page参数类型不正确或缺失')
        parser.add_argument('Size', location='args', required=False, help='Size参数类型不正确或缺失')

        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)

        res = TestController.get(**kwargs)
        if res['code'] == RET.OK:
            return jsonify(code=res['code'], message=res['message'], data=res['data'], totalPage=res['totalPage'], totalCount=res['totalCount'])
        else:
            return jsonify(code=res['code'], message=res['message'], data=res['data']) 

    # delete
    @classmethod
    def delete(cls, test_id=None):
        if test_id:
            kwargs = {
                'test_id': test_id
            }

        else:
            parser = reqparse.RequestParser()
            parser.add_argument('test_context', location='form', required=False, help='test_context参数类型不正确或缺失')
            
            # Pass in the ID list for multiple deletions
            parser.add_argument('test_id', type=str, location='form', required=False, help='test_id参数类型不正确或缺失')

            kwargs = parser.parse_args()
            kwargs = commons.put_remove_none(**kwargs)

        res = TestController.delete(**kwargs)

        return jsonify(code=res['code'], message=res['message'], data=res['data'])

    # put
    @classmethod
    def put(cls, test_id):
        if not test_id:
            return jsonify(code=RET.NODATA, message='primary key missed', error='primary key missed')

        parser = reqparse.RequestParser()
        parser.add_argument('test_context', location='form', required=False, help='test_context参数类型不正确或缺失')
        
        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)
        kwargs['test_id'] = test_id

        res = TestController.update(**kwargs)

        return jsonify(code=res['code'], message=res['message'], data=res['data'])

    # add
    @classmethod
    def post(cls):
        '''
        TestList: Pass in values in JSON format to batch add
        eg.[{k1:v1,k2:v2,...},...]
        '''
        parser = reqparse.RequestParser()
        parser.add_argument('TestList', type=str, location='form', required=False, help='TestList参数类型不正确或缺失')

        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)

        if kwargs.get('TestList'):
            res = TestController.add_list(**kwargs)

        else:
            parser.add_argument('test_id', location='form', required=True, help='test_id参数类型不正确或缺失')
            parser.add_argument('test_context', location='form', required=False, help='test_context参数类型不正确或缺失')
            
            kwargs = parser.parse_args()
            kwargs = commons.put_remove_none(**kwargs)

            res = TestController.add(**kwargs)

        return jsonify(code=res['code'], message=res['message'], data=res['data'])
