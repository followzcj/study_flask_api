#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import studentselectedcourse_blueprint
from api_1_1.studentSelectedCourseResource.studentSelectedCourseResource import StudentSelectedCourseResource
from api_1_1.studentSelectedCourseResource.studentSelectedCourseOtherResource import StudentSelectedCourseOtherResource

api = Api(studentselectedcourse_blueprint)

api.add_resource(StudentSelectedCourseResource, '/studentSelectedCourse', endpoint='StudentSelectedCourse')

#学生选课
@studentselectedcourse_blueprint.route('/studentSelectedCourse/selectcourse',methods=['POST'],endpoint='select_course')
def seclet_course():
    return StudentSelectedCourseOtherResource.student_select_course()
