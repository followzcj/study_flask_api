#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import jsonify,g
from flask_restful import Resource, reqparse

from service.studentSelectedCourseService import StudentSelectedCourseService

from utils import commons
from utils.response_code import RET


class StudentSelectedCourseOtherResource(Resource):

	#学生选课
	@classmethod
	def student_select_course(cls):
		# 接参数并校验
		parser = reqparse.RequestParser()
		parser.add_argument('courseID', type=str, location='form', required=True, help='account参数类型不正确或缺失')

		kwargs = parser.parse_args()
		kwargs = commons.put_remove_none(**kwargs)
		guserID = g.user.get('userID')
		kwargs['userID'] = guserID

		result_dict = StudentSelectedCourseService.select_course(**kwargs)

		if result_dict['code'] == RET.OK:
			return jsonify(code=result_dict['code'], message=result_dict['message'], data=result_dict['data'])
		else:
			return jsonify(code=result_dict['code'], message=result_dict['message'], error=result_dict['error'])
