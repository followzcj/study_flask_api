#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import student_blueprint
from api_1_1.studentResource.studentResource import StudentResource
from api_1_1.studentResource.studentOtherResource import StudentOtherResource

api = Api(student_blueprint)

api.add_resource(StudentResource, '/student/<autoID>', '/student', endpoint='Student')

