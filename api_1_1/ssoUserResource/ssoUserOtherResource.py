#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import jsonify
from flask_restful import Resource, reqparse

from service.ssoUserService import SsoUserService
from utils import commons
from utils.response_code import RET

class SsoUserOtherResource(Resource):

   # 学生登录
   @classmethod
   def student_login(cls):
      # 接参数并校验
      parser = reqparse.RequestParser()
      parser.add_argument('account', type=str, location='form', required=True, help='account参数类型不正确或缺失')
      parser.add_argument('password', type=str, location='form', required=True, help='password参数类型不正确或缺失')

      kwargs = parser.parse_args()
      kwargs = commons.put_remove_none(**kwargs)

      result_dict = SsoUserService.login(**kwargs)

      if result_dict['code'] == RET.OK:
         return jsonify(code=result_dict['code'], message=result_dict['message'], data=result_dict['data'])
      else:
         return jsonify(code=result_dict['code'], message=result_dict['message'], error=result_dict['error'])
