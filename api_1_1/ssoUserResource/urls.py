#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import ssouser_blueprint
from api_1_1.ssoUserResource.ssoUserResource import SsoUserResource
from api_1_1.ssoUserResource.ssoUserOtherResource import SsoUserOtherResource

api = Api(ssouser_blueprint)

api.add_resource(SsoUserResource, '/sso-user/<autoID>', '/sso-user', endpoint='SsoUser')

# 学生登录-通过蓝图方式来定义接口
@ssouser_blueprint.route('/user/login', methods=['POST'], endpoint='user_login')
def student_login():
    return SsoUserOtherResource.student_login()