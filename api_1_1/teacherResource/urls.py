#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import teacher_blueprint
from api_1_1.teacherResource.teacherResource import TeacherResource
from api_1_1.teacherResource.teacherOtherResource import TeacherOtherResource

api = Api(teacher_blueprint)

api.add_resource(TeacherResource, '/teacher/<autoID>', '/teacher', endpoint='Teacher')

