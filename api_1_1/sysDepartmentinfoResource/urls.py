#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import sysdepartmentinfo_blueprint
from api_1_1.sysDepartmentinfoResource.sysDepartmentinfoResource import SysDepartmentinfoResource
from api_1_1.sysDepartmentinfoResource.sysDepartmentinfoOtherResource import SysDepartmentinfoOtherResource

api = Api(sysdepartmentinfo_blueprint)

api.add_resource(SysDepartmentinfoResource, '/sys-departmentinfo/<autoID>', '/sys-departmentinfo', endpoint='SysDepartmentinfo')

