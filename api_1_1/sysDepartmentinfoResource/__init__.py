#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import Blueprint

sysdepartmentinfo_blueprint = Blueprint('sysDepartmentinfo', __name__)

from . import urls
