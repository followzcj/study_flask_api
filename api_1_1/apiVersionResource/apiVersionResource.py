#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Resource
from flask import jsonify
from utils.response_code import RET
from ratelimit import limits

class ApiVersionResource(Resource):

    # get the interface of apiversion -- test
    @limits(calls=2, period=60)
    def get(self):
        back_data = {
            'version': '1.1'
        }
        return jsonify(code=RET.OK, message='OK', data=back_data)    
