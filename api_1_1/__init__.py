#!/usr/bin/env python
# -*- coding:utf-8 -*-

from .apiVersionResource import apiversion_blueprint
from .adminResource import admin_blueprint
from .studentResource import student_blueprint
from .sysClassinfoResource import sysclassinfo_blueprint
from .studentSelectedCourseResource import studentselectedcourse_blueprint
from .courseResource import course_blueprint
from .courseTimeResource import coursetime_blueprint
from .todosResource import todos_blueprint
from .ssoUserResource import ssouser_blueprint
from .sysDepartmentinfoResource import sysdepartmentinfo_blueprint
from .teacherResource import teacher_blueprint
from .sysCollegeinfoResource import syscollegeinfo_blueprint
from .testResource import test_blueprint
from .vStudentCourseScoreResource import vstudentcoursescore_blueprint

from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from utils.response_code import RET

def init_limiter(app):
    global limiter
    limiter = Limiter(
        app = app,
        key_func=get_remote_address,
        # default_limits=app.config.get('DEFAULT_LIMITS').split(',')
        default_limits=["200 per day", "50 per hour"," 60 per minute"]
    )

def init_router(app):
    from api_1_1.apiVersionResource import apiversion_blueprint
    app.register_blueprint(apiversion_blueprint, url_prefix="/api_1_1")

    # admin blueprint register
    from api_1_1.adminResource import admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix="/api_1_1")
    
    # student blueprint register
    from api_1_1.studentResource import student_blueprint
    app.register_blueprint(student_blueprint, url_prefix="/api_1_1")
    
    # sysClassinfo blueprint register
    from api_1_1.sysClassinfoResource import sysclassinfo_blueprint
    app.register_blueprint(sysclassinfo_blueprint, url_prefix="/api_1_1")
    
    # studentSelectedCourse blueprint register
    from api_1_1.studentSelectedCourseResource import studentselectedcourse_blueprint
    app.register_blueprint(studentselectedcourse_blueprint, url_prefix="/api_1_1")
    
    # course blueprint register
    from api_1_1.courseResource import course_blueprint
    app.register_blueprint(course_blueprint, url_prefix="/api_1_1")

    # courseTime blueprint register
    from api_1_1.courseTimeResource import coursetime_blueprint
    app.register_blueprint(coursetime_blueprint, url_prefix="/api_1_1")
    
    # todos blueprint register
    from api_1_1.todosResource import todos_blueprint
    app.register_blueprint(todos_blueprint, url_prefix="/api_1_1")
    
    # ssoUser blueprint register
    from api_1_1.ssoUserResource import ssouser_blueprint
    app.register_blueprint(ssouser_blueprint, url_prefix="/api_1_1")
    
    # sysDepartmentinfo blueprint register
    from api_1_1.sysDepartmentinfoResource import sysdepartmentinfo_blueprint
    app.register_blueprint(sysdepartmentinfo_blueprint, url_prefix="/api_1_1")
    
    # teacher blueprint register
    from api_1_1.teacherResource import teacher_blueprint
    app.register_blueprint(teacher_blueprint, url_prefix="/api_1_1")
    
    # sysCollegeinfo blueprint register
    from api_1_1.sysCollegeinfoResource import syscollegeinfo_blueprint
    app.register_blueprint(syscollegeinfo_blueprint, url_prefix="/api_1_1")
    
    # test blueprint register
    from api_1_1.testResource import test_blueprint
    app.register_blueprint(test_blueprint, url_prefix="/api_1_1")
    
    # vStudentCourseScore blueprint register
    from api_1_1.vStudentCourseScoreResource import vstudentcoursescore_blueprint
    app.register_blueprint(vstudentcoursescore_blueprint, url_prefix="/api_1_1")
    
