#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import sysclassinfo_blueprint
from api_1_1.sysClassinfoResource.sysClassinfoResource import SysClassinfoResource
from api_1_1.sysClassinfoResource.sysClassinfoOtherResource import SysClassinfoOtherResource

api = Api(sysclassinfo_blueprint)

api.add_resource(SysClassinfoResource, '/sys-classinfo/<autoID>', '/sys-classinfo', endpoint='SysClassinfo')

