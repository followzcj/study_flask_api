#!/usr/bin/env python
# -*- coding:utf-8 -*-

import datetime
import math
import json

from sqlalchemy import or_

from app import db
from models.sys_collegeinfo import SysCollegeinfo
from utils import commons
from utils.response_code import RET, error_map_EN
from utils.loggings import loggings


class SysCollegeinfoController(SysCollegeinfo):

    # add
    @classmethod
    def add(cls, **kwargs):
        
        try:
            model = SysCollegeinfo(
                collegeID=kwargs.get('collegeID'),
                collegeName=kwargs.get('collegeName'),
                isDelete=kwargs.get('isDelete'),
                addTime=kwargs.get('addTime'),
                
            )
            db.session.add(model)
            db.session.commit()
            results = {
                'add_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'autoID': model.autoID,
                
            }
            return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'data': results}
            
        except Exception as e:
            db.session.rollback()
            loggings.exception(1, e)
            return {'code': RET.DBERR, 'message': error_map_EN[RET.DBERR], 'data': {'error': str(e)}}
        finally:
            db.session.close()

    # get
    @classmethod
    def get(cls, **kwargs):
        try:
            filter_list = []
            if kwargs.get('autoID'):
                filter_list.append(cls.autoID == kwargs['autoID'])
            else:
                if kwargs.get('autoID') is not None:
                    filter_list.append(cls.autoID == kwargs.get('autoID'))
                if kwargs.get('collegeID'):
                    filter_list.append(cls.collegeID == kwargs.get('collegeID'))
                if kwargs.get('collegeName'):
                    filter_list.append(cls.collegeName == kwargs.get('collegeName'))
                if kwargs.get('isDelete') is not None:
                    filter_list.append(cls.isDelete == kwargs.get('isDelete'))
                if kwargs.get('addTime'):
                    filter_list.append(cls.addTime == kwargs.get('addTime'))
                

            page = int(kwargs.get('Page', 1))
            size = int(kwargs.get('Size', 10))
            
            sys_collegeinfo_info = db.session.query(cls).filter(*filter_list)
            
            count = sys_collegeinfo_info.count()
            pages = math.ceil(count / size)
            sys_collegeinfo_info = sys_collegeinfo_info.limit(size).offset((page - 1) * size).all()
   
            results = commons.query_to_dict(sys_collegeinfo_info)
            return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'totalCount': count, 'totalPage': pages, 'data': results}
            
        except Exception as e:
            loggings.exception(1, e)
            return {'code': RET.DBERR, 'message': error_map_EN[RET.DBERR], 'data': {'error': str(e)}}
        finally:
            db.session.close()

    # delete
    @classmethod
    def delete(cls, **kwargs):
        try:
            filter_list = []
            if kwargs.get('autoID'):
                primary_key_list = []
                for primary_key in str(kwargs.get('autoID')).replace(' ', '').split(','):
                    primary_key_list.append(cls.autoID == primary_key)
                filter_list.append(or_(*primary_key_list))
                
            else:
                if kwargs.get('autoID') is not None:
                    filter_list.append(cls.autoID == kwargs.get('autoID'))
                if kwargs.get('collegeID'):
                    filter_list.append(cls.collegeID == kwargs.get('collegeID'))
                if kwargs.get('collegeName'):
                    filter_list.append(cls.collegeName == kwargs.get('collegeName'))
                if kwargs.get('isDelete') is not None:
                    filter_list.append(cls.isDelete == kwargs.get('isDelete'))
                if kwargs.get('addTime'):
                    filter_list.append(cls.addTime == kwargs.get('addTime'))
                
            res = db.session.query(cls).filter(*filter_list).with_for_update()

            results = {
                'delete_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'autoID': []
            }
            for query_model in res.all():
                results['autoID'].append(query_model.autoID)

            res.delete()
            db.session.commit()

            return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'data': results}

        except Exception as e:
            db.session.rollback()
            loggings.exception(1, e)
            return {'code': RET.DBERR, 'message': error_map_EN[RET.DBERR], 'data': {'error': str(e)}}
        finally:
            db.session.close()

    # update
    @classmethod
    def update(cls, **kwargs):
        try:
            
            
            filter_list = []
            filter_list.append(cls.autoID == kwargs.get('autoID'))
            
            res = db.session.query(cls).filter(*filter_list).with_for_update()
            if res.first():
                results = {
                    'update_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'autoID': res.first().autoID,
                
                }
                
                res.update(kwargs)
                db.session.commit()
            else:
                results = {
                    'error': 'data dose not exist'
                }
            
            return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'data': results}

        except Exception as e:
            db.session.rollback()
            loggings.exception(1, e)
            return {'code': RET.DBERR, 'message': error_map_EN[RET.DBERR], 'data': {'error': str(e)}}
        finally:
            db.session.close()

    # batch add
    @classmethod
    def add_list(cls, **kwargs):
        param_list = json.loads(kwargs.get('SysCollegeinfoList'))
        model_list = []
        for param_dict in param_list:
            
            model = SysCollegeinfo(
                collegeID=param_dict.get('collegeID'),
                collegeName=param_dict.get('collegeName'),
                isDelete=param_dict.get('isDelete'),
                addTime=param_dict.get('addTime'),
                
            )
            model_list.append(model)
        
        try:
            db.session.add_all(model_list)
            db.session.commit()
            results = {
                'added_records': [],
                'add_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            }
            for model in model_list:
                added_record = {}
                added_record['autoID'] = model.autoID
                
                results['added_records'].append(added_record)
                
            return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'data': results}
            
        except Exception as e:
            db.session.rollback()
            loggings.exception(1, e)
            return {'code': RET.DBERR, 'message': error_map_EN[RET.DBERR], 'data': {'error': str(e)}}
        finally:
            db.session.close()
