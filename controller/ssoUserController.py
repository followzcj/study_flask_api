#!/usr/bin/env python
# -*- coding:utf-8 -*-

import datetime
import math
import json

from sqlalchemy import or_

from app import db
from models.sso_user import SsoUser
from utils import commons
from utils.aes_encrypt_decrypt import AESEncryptDecrypt
from utils.response_code import RET, error_map_EN
from utils.loggings import loggings
from utils.rsa_encryption_decryption import RSAEncryptionDecryption
from flask import g


class SsoUserController(SsoUser):

    # add
    @classmethod
    def add(cls, **kwargs):
        # 通过定义在utils中的ID生成器函数来生成userID
        from utils.generate_id import GenerateID
        userID = GenerateID.create_random_id()
        try:
            model = SsoUser(
                userID=userID,
                account=AESEncryptDecrypt.encrypt(kwargs.get('account')) if kwargs.get('account') else None,
                password=RSAEncryptionDecryption.encrypt(kwargs.get('password')) if kwargs.get('password') else None,
                userType=kwargs.get('userType'),
                status=kwargs.get('status'),
                addTime=kwargs.get('addTime'),
                
            )
            print(model)
            db.session.add(model)
            db.session.commit()
            results = {
                'add_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'autoID': model.autoID,
                
            }
            return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'data': results}
            
        except Exception as e:
            db.session.rollback()
            loggings.exception(1, e)
            return {'code': RET.DBERR, 'message': error_map_EN[RET.DBERR], 'data': {'error': str(e)}}
        finally:
            db.session.close()

    # get
    @classmethod
    def get(cls, **kwargs):
        try:
            filter_list = []
            if kwargs.get('autoID'):
                filter_list.append(cls.autoID == kwargs['autoID'])
            else:
                if kwargs.get('autoID') is not None:
                    filter_list.append(cls.autoID == kwargs.get('autoID'))
                if kwargs.get('userID'):
                    filter_list.append(cls.userID == kwargs.get('userID'))
                if kwargs.get('account'):
                    filter_list.append(cls.account == kwargs.get('account'))
                if kwargs.get('password'):
                    filter_list.append(cls.password == kwargs.get('password'))
                if kwargs.get('userType') is not None:
                    filter_list.append(cls.userType == kwargs.get('userType'))
                if kwargs.get('status') is not None:
                    filter_list.append(cls.status == kwargs.get('status'))
                if kwargs.get('addTime'):
                    filter_list.append(cls.addTime == kwargs.get('addTime'))
                

            page = int(kwargs.get('Page', 1))
            size = int(kwargs.get('Size', 10))
            if size <= 0 or size > 100:
                size = 10

            sso_user_info = db.session.query(cls).filter(*filter_list)
            
            count = sso_user_info.count()
            pages = math.ceil(count / size)
            sso_user_info = sso_user_info.limit(size).offset((page - 1) * size).all()
   
            results = commons.query_to_dict(sso_user_info)
            if g.user.get('userType') not in [3]:
                filter_res = []
                print(type(results))
                for res in results:
                    oneres = {
                        'account':res['account'],
                        'userID':res['userID'],
                        'autoID':res['autoID']
                    }
                    filter_res.append(oneres.copy())
                return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'totalCount': count, 'totalPage': pages, 'data': filter_res}
            return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'totalCount': count, 'totalPage': pages, 'data': results}
            
        except Exception as e:
            loggings.exception(1, e)
            return {'code': RET.DBERR, 'message': error_map_EN[RET.DBERR], 'data': {'error': str(e)}}
        finally:
            db.session.close()

    # delete
    @classmethod
    def delete(cls, **kwargs):
        try:
            filter_list = []
            if kwargs.get('autoID'):
                primary_key_list = []
                for primary_key in str(kwargs.get('autoID')).replace(' ', '').split(','):
                    primary_key_list.append(cls.autoID == primary_key)
                filter_list.append(or_(*primary_key_list))
                
            else:
                if kwargs.get('autoID') is not None:
                    filter_list.append(cls.autoID == kwargs.get('autoID'))
                if kwargs.get('userID'):
                    filter_list.append(cls.userID == kwargs.get('userID'))
                if kwargs.get('account'):
                    filter_list.append(cls.account == kwargs.get('account'))
                if kwargs.get('password'):
                    filter_list.append(cls.password == kwargs.get('password'))
                if kwargs.get('userType') is not None:
                    filter_list.append(cls.userType == kwargs.get('userType'))
                if kwargs.get('status') is not None:
                    filter_list.append(cls.status == kwargs.get('status'))
                if kwargs.get('addTime'):
                    filter_list.append(cls.addTime == kwargs.get('addTime'))
                
            res = db.session.query(cls).filter(*filter_list).with_for_update()

            results = {
                'delete_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'autoID': []
            }
            for query_model in res.all():
                results['autoID'].append(query_model.autoID)

            res.delete()
            db.session.commit()

            return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'data': results}

        except Exception as e:
            db.session.rollback()
            loggings.exception(1, e)
            return {'code': RET.DBERR, 'message': error_map_EN[RET.DBERR], 'data': {'error': str(e)}}
        finally:
            db.session.close()

    # update
    @classmethod
    def update(cls, **kwargs):
        try:
            
            
            filter_list = []
            filter_list.append(cls.autoID == kwargs.get('autoID'))
            
            res = db.session.query(cls).filter(*filter_list).with_for_update()
            if res.first():
                results = {
                    'update_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    'autoID': res.first().autoID,
                
                }
                
                res.update(kwargs)
                db.session.commit()
            else:
                results = {
                    'error': 'data dose not exist'
                }
            
            return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'data': results}

        except Exception as e:
            db.session.rollback()
            loggings.exception(1, e)
            return {'code': RET.DBERR, 'message': error_map_EN[RET.DBERR], 'data': {'error': str(e)}}
        finally:
            db.session.close()

    # batch add
    @classmethod
    def add_list(cls, **kwargs):
        param_list = json.loads(kwargs.get('SsoUserList'))
        model_list = []
        for param_dict in param_list:
            
            model = SsoUser(
                userID=param_dict.get('userID'),
                account=param_dict.get('account'),
                password=param_dict.get('password'),
                userType=param_dict.get('userType'),
                status=param_dict.get('status'),
                addTime=param_dict.get('addTime'),
                
            )
            model_list.append(model)
        
        try:
            db.session.add_all(model_list)
            db.session.commit()
            results = {
                'added_records': [],
                'add_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            }
            for model in model_list:
                added_record = {}
                added_record['autoID'] = model.autoID
                
                results['added_records'].append(added_record)
                
            return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'data': results}
            
        except Exception as e:
            db.session.rollback()
            loggings.exception(1, e)
            return {'code': RET.DBERR, 'message': error_map_EN[RET.DBERR], 'data': {'error': str(e)}}
        finally:
            db.session.close()
