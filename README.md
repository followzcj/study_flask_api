# study_flask_api

#### 介绍
基于python语言flask框架的OWASP API Security Top 10测试项目


#### 安装教程

1.  根目录运行命令安装依赖包

```
pip install -r requirements.txt
```
2.  下载study_flask_api.sql文件转储至自己的数据库中
3.  修改config/develop_config.conf中的配置
4.  命令行输入`python manage.py runserver`启动服务

详细使用说明见：
https://note.youdao.com/s/NJqLgLNU


1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
