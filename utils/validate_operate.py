#!/usr/bin/env python
# -*- coding:utf-8 -*-

# file:validate_operate.py
# author:ZCJ
# datetime:2023-06-02 19:22
# software: PyCharm

"""
this is function  description 
"""

# import module your need
"""
   越权访问验证函数
"""
from functools import wraps

from flask import g, jsonify, current_app
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

from .response_code import RET, error_map_EN

# 定义一个装饰器: 用户角色验证（解决垂直越权问题：验证用户的类型是否有权操作调用接口）
def verify_user_role(role_list):
    """
    :param role_list: List[int] or Set[int] or Tuple[int] 允许访问的角色代码
    获取装饰器的传参
    """
    def _verify_userRole(func):
        """
        获取函数
        """
        @wraps(func)
        def wrapper(*func_args, **func_kwargs):
            """
            获取函数传参
            """
            if g.user.get("userType") not in role_list:
                return jsonify(code=RET.ROLEERR, message=error_map_EN[RET.ROLEERR], data={"error": "您的操作权限不足"})
            return func(*func_args, **func_kwargs)

        return wrapper

    return _verify_userRole

# 定义一个装饰器: 用户操作对象(资源)和身份是否匹配--验证操作的资源是否属于当前用户
def verify_operator_object(role_list,controller):
    def _verify_userRole(func):
        """
        获取函数
        """
        @wraps(func)
        def wrapper(*func_args, **func_kwargs):
            """
            获取函数传参
            """
            if g.user.get("userType") in role_list:
                res = controller.get(**func_kwargs)
                if res.get('code')  == RET.OK and res.get('totalCount')>0:
                    print(list(res.get('data')[0].values()))
                    print(g.user.get("userID"))
                    if str(g.user.get("userID")) not in list(res.get('data')[0].values()):
                        return jsonify(code=RET.ROLEERR, message=error_map_EN[RET.ROLEERR], data={"error": "您无权操作此对象"})
                return func(*func_args, **func_kwargs)

        return wrapper

    return _verify_userRole