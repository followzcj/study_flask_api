#!/usr/bin/env python
# -*- coding:utf-8 -*-

# file:put_remove_none.py
# author:ZCJ
# datetime:2023-06-02 20:49
# software: PyCharm

"""
this is function  description 
"""

# import module your need
def put_remove_none(field=[], **args):
    """
    PUT方法更新时，如果参数不是必填，用reqparse检验参数会将数据转为空，
    数据库有的字段可能不允许为空，故用该方法解决
    :param args: 原始数据字典
    :field: 不进行长度校验的字段集合
    :return: 除去None数据的字典
    """
    for key in list(args.keys()):
        # 空参校验
        if args[key] is None or args[key] == '':
            del args[key]
            continue
        # 长度校验
        max_length = 1000
        max_size = 1024
        import sys
        if key not in field:
            if len(bytes(args[key], encoding='utf8')) > max_length or sys.getsizeof(key) > max_size:
                del args[key]
    args = dict(args)
    print(args)
    return args