# coding: utf-8
from . import db, BaseModel


class CourseTime(BaseModel):
    __tablename__ = 'course_time'

    autoID = db.Column(db.Integer, primary_key=True)
    courseID = db.Column(db.String(20), info='课程编号')
    term = db.Column(db.Integer, info='1代表上半学期，2代表下半学期')
    classtime = db.Column(db.String(20), info='上课时间，例:1-2,3-4,5-6,7-8')
