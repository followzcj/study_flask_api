# coding: utf-8
from . import db, BaseModel


class StudentSelectedCourse(BaseModel):
    __tablename__ = 'student_selected_course'

    studentID = db.Column(db.String(20, 'utf8mb4_general_ci'), primary_key=True, nullable=False, info='学号')
    courseID = db.Column(db.String(15, 'utf8mb4_general_ci'), primary_key=True, nullable=False, info='课程编号')
    score = db.Column(db.Integer, info='成绩')
    addTime = db.Column(db.DateTime, server_default=db.FetchedValue(), info='记录添加时间')
