# coding: utf-8
from . import db, BaseModel


class Todos(BaseModel):
    __tablename__ = 'todos'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    status = db.Column(db.Integer)
