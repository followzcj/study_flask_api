# coding: utf-8
from . import db, BaseModel


class Test(BaseModel):
    __tablename__ = 'test'

    test_id = db.Column(db.Integer, primary_key=True)
    test_context = db.Column(db.String(255))
