#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
   入口程序
"""

from app import create_app
from flask_script import Manager, Server
from flask import request, jsonify, g
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from utils.response_code import RET
from utils.loggings import loggings

# 创建flask的app对象
app = create_app("develop")

# 通过Flask-Script的Manager,Server接管Flask运行
manager = Manager(app)

# 开启Debug模式
manager.add_command("runserver", Server(use_debugger=True))


# 创建全站拦截器,每个请求之前做处理
@app.before_request
def user_validation():
    print(request.endpoint)  # 方便跟踪调试
    
    if not request.endpoint: # 如果请求点为空
        return jsonify(code=RET.URLNOTFOUND, message="url not found", error="url not found")
        
@app.before_request
def user_require_token():
    # 不需要token验证的请求点列表
    # permission = ['apiversion.Apiversion', 'admin.Admin', 'student.Student', 'sysClassinfo.SysClassinfo',
    #               'studentSelectedCourse.StudentSelectedCourse', 'course.Course', 'todos.Todos',
    #               'sysDepartmentinfo.SysDepartmentinfo', 'teacher.Teacher', 'sysCollegeinfo.SysCollegeinfo',
    #               'test.Test', 'vStudentCourseScore.VStudentCourseScoreQuery', 'ssoUser.user_login',
    #               'ssoUser.SsoUser','courseTime.CourseTime']

    permission = ['ssoUser.user_login']

    add_permission = ['apiversion.Apiversion','apiversion_2.Apiversion','studentSelectedCourse_2.StudentSelectedCourse']

    permission.extend(add_permission)

    # 如果不是请求上述列表中的接口，需要验证token
    if request.endpoint not in permission:
        # 在请求头上拿到token
        token = request.headers.get("Token")
        if not all([token]):
            return jsonify(code=RET.PARAMERR, message="缺少参数Token或请求非法")

        # 校验token格式正确与过期时间
        s = Serializer(app.config["SECRET_KEY"])
        try:
            data = s.loads(token)
            g.user = data
        except Exception as e:
            app.logger.error(e)
            # 单平台用户登录失效
            return jsonify(code=RET.SESSIONERR, message='用户未登录或登录已过期')


# 创建全站拦截器，每个请求之后根据请求方法统一设置返回头
@app.after_request
def process_response(response):
    allow_cors = ['OPTIONS', 'PUT', 'DELETE', 'GET', 'POST']
    if request.method in allow_cors:
        response.headers["Access-Control-Allow-Origin"] = '*'
        if request.headers.get('Origin') and request.headers['Origin'] == 'http://api.youwebsite.com':
            response.headers["Access-Control-Allow-Origin"] = 'http://api.youwebsite.com'

        response.headers["Access-Control-Allow-Credentials"] = 'true'
        response.headers['Access-Control-Allow-Methods'] = 'OPTIONS,GET,POST,PUT,DELETE'
        response.headers['Access-Control-Allow-Headers'] = 'x-requested-with,content-type,Token,Authorization'
        response.headers['Access-Control-Expose-Headers'] = 'VerifyCodeID,ext'
    return response

@app.after_request
def check_status_code(response):
    from flask import jsonify
    # 捕获flask_limiter的频率限制返回,ratelimit暂时无法捕获
    if response.status == '429 TOO MANY REQUESTS':
        print(response.status)
        response = {"code": RET.REQERR, "error": "该ip一定时间内请求过多,暂时拒绝访问", "message": "该ip一定时间内请求过多,暂时拒绝访问"}
        response = jsonify(response)

    return response


if __name__ == "__main__":
    manager.run()

