#!/usr/bin/env python
# -*- coding:utf-8 -*-

from app import db
from models.sso_user import SsoUser
from utils.aes_encrypt_decrypt import AESEncryptDecrypt
from utils.rsa_encryption_decryption import RSAEncryptionDecryption
from utils.response_code import RET, error_map_EN
from utils.loggings import loggings
from utils import commons
from flask import g

class SsoUserService(SsoUser):
    @classmethod
    def login(cls, **kwargs):
        # 接收参数
        # filter_list = []
        user_account = kwargs.get('account')
        user_password = kwargs.get('password')
        filter_list = [cls.account == AESEncryptDecrypt.encrypt(user_account)]
        # filter_list.append(cls.account == AESEncryptDecrypt.encrypt(user_account))
        # 从数据库根据登录账号查询用户对象
        try:

            user_info = db.session.query(cls).filter(*filter_list).first()

        except Exception as e:
            loggings.exception(1, e)
            return {'code': RET.LOGINERR, 'message': error_map_EN[RET.LOGINERR], 'error': "数据库连接异常1，登录失败"}

        # 验证用户是否存在
        if not user_info:
            return {'code': RET.NODATA, 'message': error_map_EN[RET.NODATA], 'error': '用户不存在'}

        user_info_data = commons.query_to_dict(user_info)

        # 校验密码
        try:
            if RSAEncryptionDecryption.decrypt(user_info_data.get('password')) != user_password:
                return {'code': RET.LOGINERR, 'message': error_map_EN[RET.LOGINERR], 'error': "密码错误"}
        except Exception as e:
            loggings.exception(1, e)
            return {'code': RET.LOGINERR, 'message': error_map_EN[RET.LOGINERR], 'error': "数据库连接异常2，密码查验失败"}

        # 生成token
        from .userTokenService import UserTokenService
        print(user_info_data)
        token = UserTokenService.create_token(user_info_data.get('userID'), user_info_data.get('userType'))

        # 通用方法返回的全部学生信息
        user_data = commons.query_to_dict(user_info)
        # 自定义接口返回的信息
        back_data = {
            'userID': user_data.get('userID'),
            'userType': user_data.get('userType'),
            'Token': token
        }

        return {'code': RET.OK, 'message': error_map_EN[RET.OK], "data": back_data}
