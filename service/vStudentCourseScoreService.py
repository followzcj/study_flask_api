#!/usr/bin/env python
# -*- coding:utf-8 -*-

import math

from app import db
from models.v_student_course_score import t_v_student_course_score
from utils import commons
from utils.loggings import loggings
from utils.response_code import RET, error_map_EN


class VStudentCourseScoreService(object):

    @classmethod
    def joint_query(cls, **kwargs):
    
        page = int(kwargs.get('Page', 1))
        size = int(kwargs.get('Size', 10))

        filter_list = []
    

        try:
            vstudentcoursescore_info = db.session.query(t_v_student_course_score).filter(*filter_list)
            
            count = vstudentcoursescore_info.count()
            pages = math.ceil(count / size)
            vstudentcoursescore_info = vstudentcoursescore_info.limit(size).offset((page - 1) * size).all()
            results = commons.query_to_dict(vstudentcoursescore_info)

            return {'code': RET.OK, 'message': error_map_EN[RET.OK], 'totalCount': count, 'totalPage': pages, 'data': results}
    
        except Exception as e:
            loggings.exception(1, e)
            return {'code': RET.DBERR, 'message': error_map_EN[RET.DBERR], 'data': {'error': str(e)}}
            
        finally:
            db.session.close()
