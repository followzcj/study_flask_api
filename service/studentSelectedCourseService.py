#!/usr/bin/env python
# -*- coding:utf-8 -*-

from controller.studentController import StudentController
from controller.courseController import CourseController
from controller.studentSelectedCourseController import StudentSelectedCourseController
from controller.courseTimeController import CourseTimeController

from utils.response_code import RET,error_map_EN
from utils.commons import dict_to_list,dict_to_tuple


class StudentSelectedCourseService(StudentSelectedCourseController):
    
    # 学生选课
    @classmethod
    def select_course(cls,**kwargs):
        # 获取学号-课程号信息
        user_id = kwargs.get('userID')
        course_id = kwargs.get('courseID')
        get_user_info = {'userID':user_id}
        get_course_info = {'courseID':course_id}

        # 学生信息
        student_info= StudentController.get(**get_user_info)['data'][0]
        student_id = student_info['studentID']
        get_selected_class = {'studentID':student_id}
        # 课程信息
        course_info = CourseController.get(**get_course_info)['data'][0]
        # 课程时间安排
        the_class_time = CourseTimeController.get(**get_course_info)['data'][0]
        # 组合该门课程的上课时间信息为元组
        the_class_time_tuple = (the_class_time['term'], the_class_time['classtime'])
        # 已选人数
        selected_info = StudentSelectedCourseController.get(**get_course_info)
        # 课程容纳人数
        full_count = course_info['count']
        # 获取所有选择该课的studentID
        student_ids = dict_to_list('studentID',selected_info['data'])
        # 获取学生已选择课程
        selected_class = StudentSelectedCourseController.get(**get_selected_class)['data']
        student_selected = dict_to_list('courseID',selected_class)

        # 判断学生是否已经选择该门课程
        if student_info['studentID'] in student_ids:
            return {'code': RET.DATAERR, 'message': error_map_EN[RET.DATAERR], 'error': '已经选择该课程'}

        # 判断课程人数是否已满
        if selected_info['totalCount'] >= full_count:
            return {'code':RET.DATAERR,'message':error_map_EN[RET.DATAERR],'error':'课程已满'}

        # 获取已选择课程时间列表
        classes_times = []
        for the_courseid in student_selected:
            one_course_id = {'courseID':the_courseid}
            the_course_time = CourseTimeController.get(**one_course_id)['data'][0]
            temp_tuple = (the_course_time['term'],the_class_time['classtime'])
            classes_times.append(temp_tuple)

        # 判断时间是否与已选课程冲突
        if the_class_time_tuple in classes_times:
            return {'code':RET.DATAERR,'message':error_map_EN[RET.DATAERR],'error':'时间与已选课程冲突'}

        # 到这里已经排除了所有错误情况，可以返回正确信息了
        # 组合信息
        back_data = {
            'studentID': student_id,
            'courseID':course_id
        }
        StudentSelectedCourseController.add(**back_data)

        return {'code': RET.OK, 'message': error_map_EN[RET.OK], "data": back_data}