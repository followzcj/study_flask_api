#!/usr/bin/env python
# -*- coding:utf-8 -*-

# file:userTokenService.py
# author:ZCJ
# datetime:2023-06-02 17:34
# software: PyCharm

"""
this is function  description 
"""

# import module your need
from flask import current_app
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer


class UserTokenService():
    # token生成
    @classmethod
    def create_token(cls, _userid, _usertype):
        """
        生成新的token
        :param user_type: 用户类型，1--学生；2--教师；3--平台管理员；
        :param _userID:用户id
        :param _userType:用户类型（角色）
        :return: token
        """
        # 第一个参数是内部的私钥(配置信息)，第二个参数是有效期(秒)
        s = Serializer(current_app.config["SECRET_KEY"], expires_in=current_app.config["TOKEN_EXPIRES"])

        # 接受用户id转换编码
        token = s.dumps({"userID": _userid, "userType": _usertype}).decode("ascii")
        return token