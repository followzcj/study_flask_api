#!/usr/bin/env python
# -*- coding:utf-8 -*- 

from flask_restful import Resource, reqparse
from flask import jsonify

from controller.todosController import TodosController
from utils import commons
from utils.response_code import RET


class TodosResource(Resource):

    # get
    @classmethod
    def get(cls, id=None):
        if id:
            kwargs = {
                'id': id
            }

            res = TodosController.get(**kwargs)
            if res['code'] == RET.OK:
                return jsonify(code=res['code'], message=res['message'], data=res['data'])
            else:
                return jsonify(code=res['code'], message=res['message'], data=res['data'])

        parser = reqparse.RequestParser()
        parser.add_argument('title', location='args', required=False, help='title参数类型不正确或缺失')
        parser.add_argument('status', location='args', required=False, help='status参数类型不正确或缺失')
        
        parser.add_argument('Page', location='args', required=False, help='Page参数类型不正确或缺失')
        parser.add_argument('Size', location='args', required=False, help='Size参数类型不正确或缺失')

        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)

        res = TodosController.get(**kwargs)
        if res['code'] == RET.OK:
            return jsonify(code=res['code'], message=res['message'], data=res['data'], totalPage=res['totalPage'], totalCount=res['totalCount'])
        else:
            return jsonify(code=res['code'], message=res['message'], data=res['data']) 

    # delete
    @classmethod
    def delete(cls, id=None):
        if id:
            kwargs = {
                'id': id
            }

        else:
            parser = reqparse.RequestParser()
            parser.add_argument('title', location='form', required=False, help='title参数类型不正确或缺失')
            parser.add_argument('status', location='form', required=False, help='status参数类型不正确或缺失')
            
            # Pass in the ID list for multiple deletions
            parser.add_argument('id', type=str, location='form', required=False, help='id参数类型不正确或缺失')

            kwargs = parser.parse_args()
            kwargs = commons.put_remove_none(**kwargs)

        res = TodosController.delete(**kwargs)

        return jsonify(code=res['code'], message=res['message'], data=res['data'])

    # put
    @classmethod
    def put(cls, id):
        if not id:
            return jsonify(code=RET.NODATA, message='primary key missed', error='primary key missed')

        parser = reqparse.RequestParser()
        parser.add_argument('title', location='form', required=False, help='title参数类型不正确或缺失')
        parser.add_argument('status', location='form', required=False, help='status参数类型不正确或缺失')
        
        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)
        kwargs['id'] = id

        res = TodosController.update(**kwargs)

        return jsonify(code=res['code'], message=res['message'], data=res['data'])

    # add
    @classmethod
    def post(cls):
        '''
        TodosList: Pass in values in JSON format to batch add
        eg.[{k1:v1,k2:v2,...},...]
        '''
        parser = reqparse.RequestParser()
        parser.add_argument('TodosList', type=str, location='form', required=False, help='TodosList参数类型不正确或缺失')

        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)

        if kwargs.get('TodosList'):
            res = TodosController.add_list(**kwargs)

        else:
            parser.add_argument('title', location='form', required=False, help='title参数类型不正确或缺失')
            parser.add_argument('status', location='form', required=False, help='status参数类型不正确或缺失')
            
            kwargs = parser.parse_args()
            kwargs = commons.put_remove_none(**kwargs)

            res = TodosController.add(**kwargs)

        return jsonify(code=res['code'], message=res['message'], data=res['data'])
