#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import Blueprint

todos_blueprint = Blueprint('todos_2', __name__)

from . import urls
