#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import todos_blueprint
from api_1_2.todosResource.todosResource import TodosResource
from api_1_2.todosResource.todosOtherResource import TodosOtherResource

api = Api(todos_blueprint)

api.add_resource(TodosResource, '/todos/<id>', '/todos', endpoint='Todos')

