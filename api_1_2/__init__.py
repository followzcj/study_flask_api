#!/usr/bin/env python
# -*- coding:utf-8 -*-

from .apiVersionResource import apiversion_blueprint
from .adminResource import admin_blueprint
from .studentResource import student_blueprint
from .sysClassinfoResource import sysclassinfo_blueprint
from .studentSelectedCourseResource import studentselectedcourse_blueprint
from .courseResource import course_blueprint
from .courseTimeResource import coursetime_blueprint
from .todosResource import todos_blueprint
from .ssoUserResource import ssouser_blueprint
from .sysDepartmentinfoResource import sysdepartmentinfo_blueprint
from .teacherResource import teacher_blueprint
from .sysCollegeinfoResource import syscollegeinfo_blueprint
from .testResource import test_blueprint
from .vStudentCourseScoreResource import vstudentcoursescore_blueprint


def init_router(app):
    from api_1_2.apiVersionResource import apiversion_blueprint
    app.register_blueprint(apiversion_blueprint, url_prefix="/api_1_2")

    # admin blueprint register
    from api_1_2.adminResource import admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix="/api_1_2")
    
    # student blueprint register
    from api_1_2.studentResource import student_blueprint
    app.register_blueprint(student_blueprint, url_prefix="/api_1_2")
    
    # sysClassinfo blueprint register
    from api_1_2.sysClassinfoResource import sysclassinfo_blueprint
    app.register_blueprint(sysclassinfo_blueprint, url_prefix="/api_1_2")
    
    # studentSelectedCourse blueprint register
    from api_1_2.studentSelectedCourseResource import studentselectedcourse_blueprint
    app.register_blueprint(studentselectedcourse_blueprint, url_prefix="/api_1_2")
    
    # course blueprint register
    from api_1_2.courseResource import course_blueprint
    app.register_blueprint(course_blueprint, url_prefix="/api_1_2")

    # courseTime blueprint register
    from api_1_2.courseTimeResource import coursetime_blueprint
    app.register_blueprint(coursetime_blueprint, url_prefix="/api_1_2")
    
    # todos blueprint register
    from api_1_2.todosResource import todos_blueprint
    app.register_blueprint(todos_blueprint, url_prefix="/api_1_2")
    
    # ssoUser blueprint register
    from api_1_2.ssoUserResource import ssouser_blueprint
    app.register_blueprint(ssouser_blueprint, url_prefix="/api_1_2")
    
    # sysDepartmentinfo blueprint register
    from api_1_2.sysDepartmentinfoResource import sysdepartmentinfo_blueprint
    app.register_blueprint(sysdepartmentinfo_blueprint, url_prefix="/api_1_2")
    
    # teacher blueprint register
    from api_1_2.teacherResource import teacher_blueprint
    app.register_blueprint(teacher_blueprint, url_prefix="/api_1_2")
    
    # sysCollegeinfo blueprint register
    from api_1_2.sysCollegeinfoResource import syscollegeinfo_blueprint
    app.register_blueprint(syscollegeinfo_blueprint, url_prefix="/api_1_2")
    
    # test blueprint register
    from api_1_2.testResource import test_blueprint
    app.register_blueprint(test_blueprint, url_prefix="/api_1_2")
    
    # vStudentCourseScore blueprint register
    from api_1_2.vStudentCourseScoreResource import vstudentcoursescore_blueprint
    app.register_blueprint(vstudentcoursescore_blueprint, url_prefix="/api_1_2")
    
