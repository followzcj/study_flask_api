#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import Blueprint

course_blueprint = Blueprint('course_2', __name__)

from . import urls
