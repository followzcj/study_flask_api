#!/usr/bin/env python
# -*- coding:utf-8 -*- 

from flask_restful import Resource, reqparse
from flask import jsonify

from controller.studentSelectedCourseController import StudentSelectedCourseController
from controller.studentController import StudentController
from utils import commons
from utils.response_code import RET


class StudentSelectedCourseResource(Resource):

    # get
    @classmethod
    def get(cls):
        parser = reqparse.RequestParser()
        parser.add_argument('studentID', location='args', required=False, help='studentID参数类型不正确或缺失')
        parser.add_argument('courseID', location='args', required=False, help='courseID参数类型不正确或缺失')
        parser.add_argument('score', location='args', required=False, help='score参数类型不正确或缺失')
        parser.add_argument('addTime', location='args', required=False, help='addTime参数类型不正确或缺失')
        
        parser.add_argument('Page', location='args', required=False, help='Page参数类型不正确或缺失')
        parser.add_argument('Size', location='args', required=False, help='Size参数类型不正确或缺失')

        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)

        res = StudentSelectedCourseController.get(**kwargs)
        if res['code'] == RET.OK:
            return jsonify(code=res['code'], message=res['message'], data=res['data'], totalPage=res['totalPage'], totalCount=res['totalCount'])
        else:
            return jsonify(code=res['code'], message=res['message'], data=res['data']) 

    # delete
    @classmethod
    def delete(cls):
        parser = reqparse.RequestParser()
        parser.add_argument('studentID', location='form', required=True, help='studentID参数类型不正确或缺失')
        parser.add_argument('courseID', location='form', required=True, help='courseID参数类型不正确或缺失')
        
        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)

        res = StudentSelectedCourseController.delete(**kwargs)
        
        return jsonify(code=res['code'], message=res['message'], data=res['data'])

    # put
    @classmethod
    def put(cls):
        parser = reqparse.RequestParser()
        parser.add_argument('studentID', location='form', required=True, help='studentID参数类型不正确或缺失')
        parser.add_argument('courseID', location='form', required=True, help='courseID参数类型不正确或缺失')
        parser.add_argument('score', location='form', required=False, help='score参数类型不正确或缺失')
        parser.add_argument('addTime', location='form', required=False, help='addTime参数类型不正确或缺失')
        
        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)

        res = StudentSelectedCourseController.update(**kwargs)
        
        return jsonify(code=res['code'], message=res['message'], data=res['data'])

    # add
    @classmethod
    def post(cls):
        '''
        StudentSelectedCourseList: Pass in values in JSON format to batch add
        eg.[{k1:v1,k2:v2,...},...]
        '''
        parser = reqparse.RequestParser()
        parser.add_argument('StudentSelectedCourseList', type=str, location='form', required=False, help='StudentSelectedCourseList参数类型不正确或缺失')

        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)

        if kwargs.get('StudentSelectedCourseList'):
            res = StudentSelectedCourseController.add_list(**kwargs)

        else:
            parser.add_argument('studentID', location='form', required=True, help='studentID参数类型不正确或缺失')
            parser.add_argument('courseID', location='form', required=True, help='courseID参数类型不正确或缺失')
            parser.add_argument('score', location='form', required=False, help='score参数类型不正确或缺失')
            parser.add_argument('addTime', location='form', required=False, help='addTime参数类型不正确或缺失')
            
            kwargs = parser.parse_args()
            kwargs = commons.put_remove_none(**kwargs)
            guser = {'userID':"2022022813551869"}
            user = StudentController.get(**guser)
            print(user['data'])

            res = StudentSelectedCourseController.add(**kwargs)

        return jsonify(code=res['code'], message=res['message'], data=res['data'])
