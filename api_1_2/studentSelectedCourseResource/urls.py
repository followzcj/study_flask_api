#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import studentselectedcourse_blueprint
from api_1_2.studentSelectedCourseResource.studentSelectedCourseResource import StudentSelectedCourseResource
from api_1_2.studentSelectedCourseResource.studentSelectedCourseOtherResource import StudentSelectedCourseOtherResource

api = Api(studentselectedcourse_blueprint)

api.add_resource(StudentSelectedCourseResource, '/studentSelectedCourse', endpoint='StudentSelectedCourse')

