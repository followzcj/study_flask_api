#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import Blueprint

student_blueprint = Blueprint('student_2', __name__)

from . import urls
