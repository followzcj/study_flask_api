#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import ssouser_blueprint
from api_1_2.ssoUserResource.ssoUserResource import SsoUserResource
from api_1_2.ssoUserResource.ssoUserOtherResource import SsoUserOtherResource

api = Api(ssouser_blueprint)

api.add_resource(SsoUserResource, '/sso-user/<autoID>', '/sso-user', endpoint='SsoUser')

