#!/usr/bin/env python
# -*- coding:utf-8 -*- 

from flask_restful import Resource, reqparse
from flask import jsonify

from controller.courseTimeController import CourseTimeController
from utils import commons
from utils.response_code import RET


class CourseTimeResource(Resource):

    # get
    @classmethod
    def get(cls, autoID=None):
        if autoID:
            kwargs = {
                'autoID': autoID
            }

            res = CourseTimeController.get(**kwargs)
            if res['code'] == RET.OK:
                return jsonify(code=res['code'], message=res['message'], data=res['data'])
            else:
                return jsonify(code=res['code'], message=res['message'], data=res['data'])

        parser = reqparse.RequestParser()
        parser.add_argument('courseID', location='args', required=False, help='courseID参数类型不正确或缺失')
        parser.add_argument('term', location='args', required=False, help='term参数类型不正确或缺失')
        parser.add_argument('classtime', location='args', required=False, help='classtime参数类型不正确或缺失')
        
        parser.add_argument('Page', location='args', required=False, help='Page参数类型不正确或缺失')
        parser.add_argument('Size', location='args', required=False, help='Size参数类型不正确或缺失')

        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)

        res = CourseTimeController.get(**kwargs)
        if res['code'] == RET.OK:
            return jsonify(code=res['code'], message=res['message'], data=res['data'], totalPage=res['totalPage'], totalCount=res['totalCount'])
        else:
            return jsonify(code=res['code'], message=res['message'], data=res['data']) 

    # delete
    @classmethod
    def delete(cls, autoID=None):
        if autoID:
            kwargs = {
                'autoID': autoID
            }

        else:
            parser = reqparse.RequestParser()
            parser.add_argument('courseID', location='form', required=False, help='courseID参数类型不正确或缺失')
            parser.add_argument('term', location='form', required=False, help='term参数类型不正确或缺失')
            parser.add_argument('classtime', location='form', required=False, help='classtime参数类型不正确或缺失')
            
            # Pass in the ID list for multiple deletions
            parser.add_argument('autoID', type=str, location='form', required=False, help='autoID参数类型不正确或缺失')

            kwargs = parser.parse_args()
            kwargs = commons.put_remove_none(**kwargs)

        res = CourseTimeController.delete(**kwargs)

        return jsonify(code=res['code'], message=res['message'], data=res['data'])

    # put
    @classmethod
    def put(cls, autoID):
        if not autoID:
            return jsonify(code=RET.NODATA, message='primary key missed', error='primary key missed')

        parser = reqparse.RequestParser()
        parser.add_argument('courseID', location='form', required=False, help='courseID参数类型不正确或缺失')
        parser.add_argument('term', location='form', required=False, help='term参数类型不正确或缺失')
        parser.add_argument('classtime', location='form', required=False, help='classtime参数类型不正确或缺失')
        
        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)
        kwargs['autoID'] = autoID

        res = CourseTimeController.update(**kwargs)

        return jsonify(code=res['code'], message=res['message'], data=res['data'])

    # add
    @classmethod
    def post(cls):
        '''
        CourseTimeList: Pass in values in JSON format to batch add
        eg.[{k1:v1,k2:v2,...},...]
        '''
        parser = reqparse.RequestParser()
        parser.add_argument('CourseTimeList', type=str, location='form', required=False, help='CourseTimeList参数类型不正确或缺失')

        kwargs = parser.parse_args()
        kwargs = commons.put_remove_none(**kwargs)

        if kwargs.get('CourseTimeList'):
            res = CourseTimeController.add_list(**kwargs)

        else:
            parser.add_argument('courseID', location='form', required=False, help='courseID参数类型不正确或缺失')
            parser.add_argument('term', location='form', required=False, help='term参数类型不正确或缺失')
            parser.add_argument('classtime', location='form', required=False, help='classtime参数类型不正确或缺失')
            
            kwargs = parser.parse_args()
            kwargs = commons.put_remove_none(**kwargs)

            res = CourseTimeController.add(**kwargs)

        return jsonify(code=res['code'], message=res['message'], data=res['data'])
