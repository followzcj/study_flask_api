#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import Blueprint

coursetime_blueprint = Blueprint('courseTime_2', __name__)

from . import urls
