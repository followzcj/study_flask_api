#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import test_blueprint
from api_1_2.testResource.testResource import TestResource
from api_1_2.testResource.testOtherResource import TestOtherResource

api = Api(test_blueprint)

api.add_resource(TestResource, '/test/<test_id>', '/test', endpoint='Test')

