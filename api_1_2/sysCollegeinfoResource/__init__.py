#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import Blueprint

syscollegeinfo_blueprint = Blueprint('sysCollegeinfo_2', __name__)

from . import urls
