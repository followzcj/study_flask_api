#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask_restful import Api

from . import syscollegeinfo_blueprint
from api_1_2.sysCollegeinfoResource.sysCollegeinfoResource import SysCollegeinfoResource
from api_1_2.sysCollegeinfoResource.sysCollegeinfoOtherResource import SysCollegeinfoOtherResource

api = Api(syscollegeinfo_blueprint)

api.add_resource(SysCollegeinfoResource, '/sys-collegeinfo/<autoID>', '/sys-collegeinfo', endpoint='SysCollegeinfo')

